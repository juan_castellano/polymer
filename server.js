//LIBRERIA EXPRESS
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var port=process.env.PORT || 3000;
var requestJson = require('request-json');
var baseMlabURL="https://api.mlab.com/api/1/databases/apitechujco/collections/";
var mLabAPIKey="apiKey=OAcuJLU6YBy12KCVHbRGtFdGcEKpsqKG";

app.listen(port);
console.log("API escuchando en el puerto bip bip "+port)

app.get('/apitechu/v1',
 function(req,res){
   console.log("GET /apitechu/v1");
   res.send({
    "msg" :"Bienvenido a la API Tech University"
  });
 }
);

app.get('/apitechu/v1/users',
function(req,res){
  console.log("GET /apitechu/v1/users");
  res.sendFile('usuarios.json',{root: __dirname});
}
);

app.post('/apitechu/v1/users',
function(req,res){
  console.log("POST /apitechu/v1/users");
  var newUser ={
    "first_name" : req.headers.first_name,
    "last_name" : req.headers.last_name,
    "country" : req.headers.country
  };

 var users = require('./usuarios.json');
 users.push(newUser);
 writeUserDataToFile(users);
res.send({ "msg" :"FIn del POST de la API Tech University"});
}
);

app.delete('/apitechu/v1/users/:id',
  function(req,res){
    console.log("DELETE /apitchu/v1/users/:id");
    var users = require('./usuarios.json');
    users.splice(req.params.id-1,1);
    writeUserDataToFile(users);
  res.send({"msg" :"Usuario borrado"});
}
);


function writeUserDataToFile(data){
var fs=require('fs');
var jsonUserData= JSON.stringify(data);
fs.writeFile("./usuarios.json",jsonUserData,"utf8",function(err){
 if (err){
    console.log(err);
  }
  else {
    console.log("Success en fichero");
  }
});
}

app.post('/apitechu/v1/monstruo/:p1/:p2',
  function(req,res){
/*
    console.log("Parametros");
    console.log(req.params);
    console.log("Query String");
    console.log(req.query);
    console.log("Headers");
    console.log(req.headers);*/

    console.log("first name "+req.body.first_name);
    console.log("last name "+ req.body.last_name);
    console.log("country "+ req.body.country);

    var newUser ={
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "country" : req.body.country
    };

    var users = require('./usuarios.json');
    users.push(newUser);
    writeUserDataToFile(users);

    res.send({"msg" :"FIN monstruo"});
  }
);

app.post('/apitechu/v1/login',
  function(req,res){

    var logU ={
    "email" : req.body.email,
    "password" : req.body.password
    };
 var resultado=LogUser(logU,"./login.json");
   if(resultado>0){
     res.send({"msg" :"Usuario logueado OK",
                "idUsuario" : resultado});
   }
   if(resultado==-1){
     res.send({"msg" :"Email NO ENCONTRADO"});
   }
   if(resultado==0){
     res.send({"msg" :"Password Usuario incorrecto"});
   }

  }
);

function LogUser(loguser,data){
var users = require(data);
var login =-1;
for (user of users) {
      if(loguser.email==user.email&&loguser.password!=user.password){
        login=0;
        break;
      }
      if(loguser.email==user.email&&loguser.password==user.password){
        login=user.id;
        loguser.logged=true;
        users.push(loguser);
        writeUserDataToFile(users);
        break;
      }
   }
   console.log("salimos ddel bucle "+login);
   return login;
 }


 app.post('/apitechu/v1/logout',
 function(req,res){

 id=req.body.id;

 var resultado=Logout(id,"./login.json");
  if(resultado==0){
    res.send({"msg" :"Usuario logout OK",
               "idUsuario" : id});
  }else{
    res.send({"msg" :"Usuario logout ERR"});
  }
}
 );

 function Logout(loguser,data){
 var users = require(data);
 var logout =-1;
 for (user of users) {
       if(user.id==loguser&&user.logged==true){
         delete user.logged;
         logout=0;
         users.push(user);
         writeUserDataToFile(users);
         break;
       }
    }
console.log(logout);
    return logout;
  }

app.get('/apitechu/v2/users',
  function(req,res){
    console.log("GET /apitechu/v2/users");
    httpClient=requestJson.createClient(baseMlabURL);
    httpClient.get("user?"+mLabAPIKey,
      function(err, resMLab, body){
        var response = !err ? body : {
          "msg" : "Error obteniendo usuarios."
        }
        res.send(response);
      }

    );
  }
);

app.get('/apitechu/v2/users/:id',
 function(req, res) {
   console.log("GET /apitechu/v2/users/:id");

   var id = req.params.id;
   var query = 'q={"id" : ' + id + '}&f={"_id":0,"password":0}';

   httpClient = requestJson.createClient(baseMlabURL);

   httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo usuario."
         }
         res.status(500);
       } else {
         if (body.length > 0) {
           response = body[0];
         } else {
           response = {
             "msg" : "Usuario no encontrado."
           };
           res.status(404);
         }
       }
       res.send(response);
     }
   )
 }
);

//login usuario en BBDD
app.post('/apitechu/v2/login',
 function(req, res) {
   console.log("POST /apitechu/v2/login");

pass=req.body.password.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0);
var querybd = 'q={"email" : "' + req.body.email + '" ,"password":"'+ pass +'"}';
 httpClient = requestJson.createClient(baseMlabURL);
   httpClient.get("user?" + querybd + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (err) {
         var response = {
           "msg" : "Error obteniendo usuario."
         }
         res.send(response);
         res.status(500);
       } else {
         if (body.length > 0) {
           //response = body[0];
           var querybody = 'q={"id" : ' + body[0].id +'}';
           var putBody='{"$set":{"logged":true}}';
           httpClient.put("user?"+ querybody+"&"+mLabAPIKey, JSON.parse(putBody),
              function(errPUT,resMlabPUT,bodyPUT){
                  var response = { "msg" : "Usuario LOGGIN OK",
                  "idUsuario" : body[0].id,
                  "first_name" : body[0].first_name};
                  res.send(response);
              }
            );
         } else {
           var response = {"msg" : "email/password incorrecto"};
           res.send(response);
       }
       }
    }
   )
 }
);

//logout usuario
app.post('/apitechu/v2/logout/:id',
 function(req, res) {
   console.log("POST /apitechu/v2/logout/:id");

   var query = 'q={"id": ' + req.params.id + '}';

   httpClient = requestJson.createClient(baseMlabURL);
   httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (body.length == 0) {
         var response = {
           "mensaje" : "Logout incorrecto, usuario no encontrado"
         }
         res.send(response);
       } else {
         console.log("Got a user with that id, logging out");
         query = 'q={"id" : ' + body[0].id +'}';
         console.log("Query for put is " + query);
         var putBody = '{"$unset":{"logged":""}}'
         httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             console.log("PUT done");
             var response = {
               "msg" : "Usuario deslogado con éxito",
               "idUsuario" : body[0].id
             }
             res.send(response);
           }
         )
       }
     }
   );
 }
);

//ver cuentas de usuario
app.get('/apitechu/v2/users/:id/accounts',
 function(req, res) {
   console.log("GET /apitechu/v2/users/:id/accounts");

   var id = req.params.id;
   var query = 'q={"id" : ' + id + '}';

   httpClient = requestJson.createClient(baseMlabURL);

   httpClient.get("account?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo cuenta de usuario."
         }
         res.status(500);
       } else {
         if (body.length > 0) {
           response = body;
         } else {
           response = {
             "msg" : "Cuenta no encontrada."
           };
           res.status(404);
         }
       }
       res.send(response);
     }
   )
 }
);

//movimientos de una cta
app.get('/apitechu/v2/users/:iban/movments',
 function(req, res) {
   console.log("GET /apitechu/v2/users/:iban/movments");

   var id = '"'+req.params.iban+'"';
   var query = 'q={"iban" : ' + id + '}&s={"date":-1}';
   httpClient = requestJson.createClient(baseMlabURL);

   httpClient.get("movments?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo movimientos de cuenta"
         }
         res.status(500);
       } else {
         if (body.length > 0) {
           response = body;
         } else {
           response = {
             "msg" : "Cuenta no encontrada."
           };
           res.status(404);
         }
       }
       res.send(response);
     }
   )
 }
);

//alta usuario
app.post('/apitechu/v2/alta',
 function(req, res) {
   console.log("POST /apitechu/v2/alta");
    httpClient = requestJson.createClient(baseMlabURL);
   //sacar max id y sumar 1
   var query='f={"_id":0,"id": 1}&l=1&s={"id":-1}';
   httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab,body) {
    var new_id=body[0].id+1;
   //si ya existe el email error
   var queryemail = 'q={"email" : "' + req.body.email+'"}';
   httpClient = requestJson.createClient(baseMlabURL);

   httpClient.get("user?" + queryemail + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error comprobando email."
         }
         res.send(response);
         res.status(500);
       } else {

         if (body.length > 0) {
           response = { "msg" : "Email existente en nuestra Base de datos",
                        "email" : body[0].email
                      };
            res.send(response);
            res.status(500);
          }else {
           //lo damos de alta

           //password hash SHA1
           var pass=req.body.password.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0)
           var queryinsert = '{"id" :'+new_id+',"email" : "' + req.body.email + '","first_name": "' + req.body.first_name + '","last_name": "' + req.body.last_name + '","phone": "' + req.body.phone + '","address": "' + req.body.address + '","password": "' + pass+'"}';

           httpClient.post("user?&" + mLabAPIKey, JSON.parse(queryinsert),
             function(errPUT, resMLabPUT, bodyPUT) {
               if (errPUT) {
                 response = {"msg" : "Error INSERTANDO usuario."}
                 res.send(response);
                 res.status(500);
               } else {
               var response = {"msg" : "Usuario dade de alta con éxito"}
               res.send(response);
             }
           }//fin function  put
           );
        }//fin else dar alta
      }//fin else respueta email ok
    }//fin function err
);//fin primer httpclient de comprobacion de email
});//fin sacar max id
});

//baja usuario
//Borra usuario de la bbdd sino tiene cuentas asociadas

app.post('/apitechu/v2/users/borra',
 function(req, res) {
   console.log("post /apitechu/v2/users/borra");

   var id = req.body.id;
   var query = 'q={"id" : ' + id + '}';
   httpClient = requestJson.createClient(baseMlabURL);
   //vemos si tiene cuentas
   httpClient.get("account?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo cuenta de usuario."
         }
         res.send(response);
         res.status(500);
          } else {
         if (body.length > 0) {
           response = {
             "msg" : "Usuario con cuentas activas."
           };
           res.send(response);
         } else {
           //borramos usuario de la bbdd
           var putBody = '{"$unset":{"id":"","first_name": "","last_name": "","email": "","password": "","address": "","phone": "","logged":""}}'
           httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
             function(errPUT, resMLabPUT, bodyPUT) {
               var response = {
                 "msg" : "Cuenta borrada con éxito"
               }
                res.send(response);
             }//fin del put
           );//fin httpClient del put
         }
       }
     }
   )
 }
);

//edita info usuario
app.post('/apitechu/v2/users/edit',
 function(req, res) {
   console.log("post /apitechu/v2/users/edit");

   var id = req.body.id;
   var query = 'q={"id" : ' + id + '}';
   httpClient = requestJson.createClient(baseMlabURL);
   var putBody = '{"$set":{"email" : "' + req.body.email + '","first_name": "' + req.body.first_name + '","last_name": "' + req.body.last_name + '","phone": "' + req.body.phone + '","address": "' + req.body.address +'"}}';
   //comprobar que vienen los otros parametros informados
   if(req.body.email.length>0&req.body.first_name.length>0&req.body.last_name.length>0&req.body.address.length>0&req.body.phone.length>0){

   httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             }//fin del put
           );//fin httpClient del put
           var response = {
               "msg" : "Información actualizada"
           }
          res.send(response);
       }

       else{
         var response = {
             "msg" : "ERR. faltan campos"
         }
            res.send(response);
       }
});

//cambia password
app.post('/apitechu/v2/users/editpwd',
 function(req, res) {
   console.log("post /apitechu/v2/users/editpwd");
   var id = req.body.id;
   var query = 'q={"id" : ' + id + '}';
   var pass=req.body.password.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0)
   //si viene vacio error
   if (req.body.password.length>0){
   httpClient = requestJson.createClient(baseMlabURL);
   var putBody = '{"$set":{"password" : "' + pass +'"}}';
   httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             var response = {
                 "msg" : "Password cambiado"
             }
                res.send(response);
             }//fin del put
           );//fin httpClient del put
         }
    else{
      var response = {
          "msg" : "ERR. Formato Password"
      }
         res.send(response);
    }
});

//alta cuenta.
//Generamos aleatoriamente IBAN e inicializamos a 0
//requiere id usuario en body
app.post('/apitechu/v2/altacta/:id',
 function(req, res) {
   console.log("POST /apitechu/v2/altacta");
    httpClient = requestJson.createClient(baseMlabURL);
   //generamos cta aleatoriamante
    var iban = "";
    var c="";
    c=Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 4);
    iban=c;
    for (i = 0; i < 5; i++) {
      iban=iban+" "+c;
      c=Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 4);
     }
         var queryinsert = '{"id" :'+req.params.id+',"iban" : "' + iban.toUpperCase() + '","balance": 0.00}';
           httpClient.post("account?&" + mLabAPIKey, JSON.parse(queryinsert),
             function(errPUT, resMLabPUT, bodyPUT) {
               if (errPUT) {
                 response = {"msg" : "Error INSERTANDO cta."}
                 res.send(response);
                 res.status(500);
               } else {
               var response = {"msg" : "Nueva cuenta dada de alta con éxito"}
               res.send(response);
             }
           }//fin function  put
           );
        }//fin else dar alta
      );//fin primer httpclient de comprobacion de email

//baja cta, borrar cta si esta a 0€ el balance
app.post('/apitechu/v2/cancelcta',
   function(req, res) {
     console.log("POST /apitechu/v2/cancelcta");
     //recuperamos saldo
     var query = 'q={"iban": "' + req.body.iban + '"}';
     httpClient = requestJson.createClient(baseMlabURL);
     httpClient.get("account?" + query + "&" + mLabAPIKey,
         function(err, resMLab, body) {
           if (body.length == 0) {
             var response = {
               "mensaje" : "Cta no encontrada"
             }
             res.send(response);
             } else {
               var saldocta=body[0].balance;
               if(saldocta!=0){
                 var response = {
                   "msg" : "Error. Saldo de la cuenta no es 0"
                 }
                 res.send(response);
               }
               else{
                 var putBody = '{"$unset":{"id":"","iban":"","balance":""}}'
                 httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                   function(errPUT, resMLabPUT, bodyPUT) {
                     var response = {
                       "msg" : "Cuenta borrada con éxito"
                     }
                     res.send(response);
                   }//fin del put
                 );//fin httpClient del put
             }//fin else check saldo a 0
             }//fin else check si cta existe
           }//fin function account
        );//fin sacar account
       });

//movimiento entre cuentas, origen, destino, importe
//se recupera saldo en origen y se le resta el importe
//se recupera saldo en destino y se le suma el importe
//se graban ambos movimientos con fecha del sistema en tabla movimientos
//llamada por post con ctaorigen, ctadestino, importe
app.post('/apitechu/v2/transferencia',
 function(req, res) {
   console.log("POST /apitechu/v2/transferencia");
   var query = 'q={"iban" : "' + req.body.ctaorigen+'"}';
   httpClient = requestJson.createClient(baseMlabURL);
   var response = {"msg" : "Movimiento realizado correctamente"}
   httpClient.get("account?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (err) {
         response = {"msg" : "Error obteniendo saldo cuenta origen."}
         res.status(500);
       }else{

       var balance_origen=body[0].balance;
       //si el importe es mayor que el saldo no dejamos hacer la operacion
       if(balance_origen<Number(req.body.importe)){
         response = {"msg" : "Error saldo insufiente para realizar la operacion."}
         res.send(response);
       }else{
       //si no existe cta destino error
       var query = 'q={"iban" : "' + req.body.ctadestino+'"}';
       httpClient = requestJson.createClient(baseMlabURL);
       httpClient.get("account?" + query + "&" + mLabAPIKey,
         function(err, resMLab2, body) {
           if (err) {
             response = {"msg" : "Error comprobando cta destino."}
             res.status(500);
           }else{
            if (body.length==0) {
                 response = { "msg" : "Cta destino no existente en nuestra Base de datos",
                              "ctadestino" : req.body.ctadestino
                            };
            }else{
             var balance=body[0].balance;

             //procedemos a realizar el movimiento en ambas cuentas en la origen es negativo
             var date1=new Date().toISOString().slice(0,10);
             var date2 = new Date().toISOString().slice(11,19);
             var fecha_mvto=date1+" "+date2;
             var queryinsert = '{"iban" : "' + req.body.ctaorigen + '","date": "' + fecha_mvto + '","importe": ' + req.body.importe*-1 +'}';
             httpClient.post("movments?&" + mLabAPIKey, JSON.parse(queryinsert),
             function(errPUT, resMLabPUT, bodyPUT) {
                     if (errPUT) {
                       response = {"msg" : "Error grabando movimiento."}
                       res.status(500);
                     }  else{

                       var queryinsert = '{"iban" : "' + req.body.ctadestino + '","date": "' + fecha_mvto + '","importe": ' + req.body.importe +'}';
                       httpClient.post("movments?&" + mLabAPIKey, JSON.parse(queryinsert),
                         function(errPUT, resMLabPUT, bodyPUT) {
                           if (errPUT) {
                             response = {"msg" : "Error grabando movimiento."}
                             res.status(500);
                           } else {
                             //calculamos nuevos importes y actualizamos balance
                             balance_origen=balance_origen-Number(req.body.importe);
                             balance=balance+Number(req.body.importe);
                             query = 'q={"iban" : "' + req.body.ctaorigen +'"}';
                             var putBody = '{"$set":{"balance":'+balance_origen.toFixed(2)+'}}'
                             httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                               function(errPUT, resMLabPUT, bodyPUT) {
                               var response = {"msg" : "balance restado OK"}
                                 //res.send(response);
                               });//find del seteo nuevo importe en origen
                             query = 'q={"iban" : "' + req.body.ctadestino +'"}';
                             var putBody = '{"$set":{"balance":'+balance.toFixed(2)+'}}'
                             httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                               function(errPUT, resMLabPUT, bodyPUT) {
                                 var response = {"msg" : "balance sumado OK"}
                               });//fin seteo saldo cuenta destino
                             }//fin else put movimiento cta destino
                          });//fin function put movimiento cta origen
                        }//fin else graba movimiento en cta destino
       });//fin funcin graba importe nuevo en cta origen
    }//fin del else qeu encuentra cta destino
    res.send(response);
  }}//cierra funcion cta destino
);//fin recukpera balance cta destino
}//fin else primer check err en get cuenta origen
  }});//fin recupera balance cuenta origen
}//fin function primera de la api
);

//hasheo de todas las password de los usuarios de la bbdd
app.post('/apitechu/v2/hash/:id',
 function(req, res) {
  console.log("POST /apitechu/v2/hash");
    var id = req.params.id;
    var query = 'q={"id" : ' + id +'}';
    httpClient = requestJson.createClient(baseMlabURL);
      httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
        var pass=body[0].password.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0);
        var putBody='{"$set":{"password":"'+pass+'"}}';
        httpClient.put("user?"+ query +"&"+mLabAPIKey, JSON.parse(putBody),
          function(errPUT,resMlabPUT,bodyPUT){
                response = {"msg" : "ok."}
                res.send(response);
                }
          )
      }
    )
  }
);

//check password strength
app.get('/apitechu/v2/password/:password',
 function(req,res){
   console.log("GET /apitechu/v2/password");
   //Comprobamos si el password es bueno
   var taiPasswordStrength = require("tai-password-strength");
   var strengthTester = new taiPasswordStrength.PasswordStrength();
   strengthTester.addCommonPasswords(taiPasswordStrength.commonPasswords);
   strengthTester.addTrigraphMap(taiPasswordStrength.trigraphs);
   var betterResults = strengthTester.check(req.params.password);
   var response = {"msg" : betterResults.strengthCode}
   res.send(response);
 }
);
