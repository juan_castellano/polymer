# imagen raiz
FROM node

#carpeta raiz
WORKDIR /esquema_proyecto

#copia de archivos
ADD . /esquema_proyecto

#Añadir volumen
#VOLUME ['/logs']

#Exponer puerto
EXPOSE 3000

#instalar dependencias
#RUN npm install

# cOMANDO DE INICIALIZACION
CMD ["npm", "start"]
