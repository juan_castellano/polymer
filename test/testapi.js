var mocha = require('mocha');
var chai = require('chai');
var chaihttp = require('chai-http');

var server = require('../server.js');

chai.use(chaihttp);

var should= chai.should();

describe('First test',
  function(){
    it('Test that as.com works', function(done){
      chai.request('http://www.as.com').get('/').end(
        function(err,res){
          console.log("Request has ended");
          //console.log(res);
          console.log(err);
          res.should.have.status(200);
          done();
        }
      )
    }
  );

  }
);

describe('2nd test',
  function(){
    it('Test that apitechu works', function(done){
      chai.request('http://localhost:3000').get('/apitechu/v1').end(
        function(err,res){
          res.should.have.status(200);
          res.body.msg.should.be.eql("Bienvenido a la API Tech University")
          done();
        }
      )
    }
  );
  it('Prueba que la API devuelve una lista de usuarios correctos.',
     function(done) {
       chai.request('http://localhost:3000')
       .get('/apitechu/v1/users')
       .end(
         function(err, res) {
           res.should.have.status(200);
           //respuesta que sea un array
           res.body.should.be.a("array");
           for (user of res.body) {
             user.should.have.property('first_name');
             user.should.have.property('last_name');
           }
           done();
         }
       )
     }
   )
  }
);
